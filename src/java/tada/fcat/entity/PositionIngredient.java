/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tada.fcat.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

/**
 *
 * @author kudlaty524
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "getIngredientByPositionNameAndIngredientName", query = "SELECT i FROM PositionIngredient i WHERE i.position.positionName = :positonName AND i.ingredientName = :ingredientName"),
    @NamedQuery(name = "getIngredientsByPosition", query = "SELECT i FROM PositionIngredient i WHERE i.position.positionName = :positonName")
})
public class PositionIngredient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    protected MenuPosition position;

    @NotNull
    protected String ingredientName;

    @NotNull
    protected int grams;

    public PositionIngredient() {
    }

    public PositionIngredient(String ingredientName, int grams, MenuPosition mp) {
        this.ingredientName = ingredientName;
        this.grams = grams;
        this.position = mp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MenuPosition getPosition() {
        return position;
    }

    public void setPosition(MenuPosition position) {
        this.position = position;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public int getGrams() {
        return grams;
    }

    public void setGrams(int grams) {
        this.grams = grams;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PositionIngredient)) {
            return false;
        }
        PositionIngredient other = (PositionIngredient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tada.fcat.entity.PositionIngredient[ id=" + id + " ]";
    }

}
