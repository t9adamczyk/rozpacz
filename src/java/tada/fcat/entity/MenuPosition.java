/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tada.fcat.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kudlaty524
 */
@Entity
@NamedQueries({
@NamedQuery(name = "getPositionByPositionName", query = "SELECT p FROM MenuPosition p WHERE p.positionName = :positionName"),
@NamedQuery(name = "getPositionsByCategoryName", query = "SELECT p FROM MenuPosition p WHERE p.menuCategory.categoryName = :menuCategory")
})
public class MenuPosition implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(unique = true)
    private String positionName;
    
    @ManyToOne
    @JoinColumn(name="categoryName")
    protected MenuCategory menuCategory;

    @OneToMany(cascade=ALL, mappedBy="position")
    Collection<PositionIngredient> ingredients;
    
    @Lob
    @Basic(fetch=FetchType.LAZY)
    @XmlTransient
    private byte[] imgSrc;

    public MenuPosition() { 
    }

    public MenuPosition(String positionName, MenuCategory menuCategory, byte[] imgSrc) {
        this.positionName = positionName;
        this.menuCategory = menuCategory; 
        this.imgSrc = imgSrc;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }
    
    public Collection<PositionIngredient> getIngredients() {
        return ingredients;
    }
    
    


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (positionName != null ? positionName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the positionName fields are not set
        if (!(object instanceof MenuPosition)) {
            return false;
        }
        MenuPosition other = (MenuPosition) object;
        if ((this.positionName == null && other.positionName != null) || (this.positionName != null && !this.positionName.equals(other.positionName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tada.fcat.entity.MenuPosition[ id=" + positionName + " ]";
    }
    
}
