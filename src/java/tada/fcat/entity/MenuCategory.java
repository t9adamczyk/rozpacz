/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tada.fcat.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
/**
 *
 * @author kudlaty524
 */
@Entity
@NamedQueries({
@NamedQuery(name = "getCategoryByCategoryName", query = "SELECT c FROM MenuCategory c WHERE c.categoryName = :categoryName"),
@NamedQuery(name = "getAllCategories", query = "SELECT c FROM MenuCategory c")
})


public class MenuCategory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(unique = true)
    private String categoryName;
    @OneToMany(cascade=ALL, mappedBy="menuCategory")
    protected Collection<MenuPosition> menuPositionInCat;
;

    public Collection<MenuPosition> getMenuPositionInCat() {
        return menuPositionInCat;
    }

    public void setMenuPositionInCat(Collection<MenuPosition> menuPositionInCat) {
        this.menuPositionInCat = menuPositionInCat;
    }
    
    public MenuCategory (){
        this.menuPositionInCat = new LinkedList<>();
    }
    
    public MenuCategory (String categoryName){
        this.menuPositionInCat = new LinkedList<>();
        setCategoryName(categoryName);
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoryName != null ? categoryName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the categoryName fields are not set
        if (!(object instanceof MenuCategory)) {
            return false;
        }
        MenuCategory other = (MenuCategory) object;
        if ((this.categoryName == null && other.categoryName != null) || (this.categoryName != null && !this.categoryName.equals(other.categoryName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tada.fcat.entity.Category[ id=" + categoryName + " ]";
    }
    
}
