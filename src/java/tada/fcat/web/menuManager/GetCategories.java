/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tada.fcat.web.menuManager;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;
import tada.fcat.ejb.CategoriesManipulator;
import tada.fcat.entity.MenuCategory;

/**
 *
 * @author kudlaty524
 */
@Named(value = "getCategories")
@SessionScoped
public class GetCategories implements Serializable{

    @EJB
    private CategoriesManipulator cm;
    
    /**
     * Creates a new instance of GetCategories
     */
    public GetCategories() {
    }
    
    public HashMap<String,String> getCategoriesMap(){
        HashMap<String,String> categories = new HashMap<>();
        final List<MenuCategory> categoriesList = cm.getCategories();
        for (MenuCategory category : categoriesList) {
            categories.put(category.getCategoryName(), category.getCategoryName());
        }
        return categories;
    }
}
