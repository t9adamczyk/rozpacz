/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tada.fcat.web.menuManager;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import tada.fcat.ejb.CategoriesManipulator;
import tada.fcat.entity.MenuPosition;

/**
 *
 * @author kudlaty524
 */
@Named(value = "addIngredientDialog")
@SessionScoped
public class AddIngredientDialog implements Serializable{

    private MenuPosition menuPositionForDialog;

    @EJB
    CategoriesManipulator manipulator;

    /**
     * Creates a new instance of IngredientDialog
     */
    public AddIngredientDialog() {
    }

    private String ingredientName;
    private int weightInGrams;

    public void addIngredientDialog(MenuPosition menuPositionForDialog) {
        this.menuPositionForDialog = menuPositionForDialog;
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
        options.put("contentHeight", 200); 
        RequestContext.getCurrentInstance().openDialog("addIngredient", options, null);
    }

    public void addIngredientToPositionForDialog() {
        if (!manipulator.ifIngredientWithPositionNameAndIngredientNameExists(menuPositionForDialog.getPositionName(),ingredientName)){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Składnk o tej nazwie już istnieje", ""));
        } else {
        manipulator.addIngredient(ingredientName, weightInGrams, menuPositionForDialog.getPositionName());
        RequestContext.getCurrentInstance().closeDialog(null);
        }
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public int getWeightInGrams() {
        return weightInGrams;
    }

    public void setWeightInGrams(int weightInGrams) {
        this.weightInGrams = weightInGrams;
    }

    public MenuPosition getMenuPositionForDialog() {
        return menuPositionForDialog;
    }
}
