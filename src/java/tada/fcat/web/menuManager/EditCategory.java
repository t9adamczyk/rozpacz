/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tada.fcat.web.menuManager;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import tada.fcat.ejb.CategoriesManipulator;
import tada.fcat.entity.MenuCategory;
import tada.fcat.entity.MenuPosition;

/**
 *
 * @author kudlaty524
 */
@Named(value = "editCategory")
@SessionScoped
public class EditCategory implements Serializable {

    @EJB
    CategoriesManipulator manipulator;
    private static final Logger logger = Logger.getLogger(AddCategory.class.getName());

    private String categoryName;
    private String positionNameInDialog;

    private String tmpCategoryNameInDialog;

    private byte[] imgSrc;

    /**
     * Creates a new instance of EditCategory
     */
    public EditCategory() {
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<MenuPosition> getPositions() {
        return manipulator.getPositionsByCategoryName(categoryName);
    }

    public void addPositionDialog() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("modal", true);
        options.put("contentHeight", 100); 
        RequestContext.getCurrentInstance().openDialog("addPosition", options, null);
    }

    public void renameCategoryDialog() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("modal", true);
        options.put("contentHeight", 100); 
        RequestContext.getCurrentInstance().openDialog("renameCategory", options, null);
    }

    public void removePosition(MenuPosition menuPosition) {
        if (menuPosition != null) {
            manipulator.removePosition(menuPosition);
            logger.log(Level.INFO, "Removed " + menuPosition.getPositionName());
        } else {
            logger.log(Level.INFO, "Position do not exist. Not removed");
        }
    }

    public void removeCategory() {
        if (manipulator.getCategory(categoryName) != null) {
            manipulator.removeCategory(categoryName);
            logger.log(Level.INFO, "Removed {0}", categoryName);
            categoryName = null;
        } else {
            logger.log(Level.INFO, "Not removed {0}", categoryName);
        }

        List<MenuCategory> lcm = manipulator.getCategories();
        for (MenuCategory mc : lcm) {
            logger.log(Level.INFO, mc.toString());
        }
    }

    public void renameCategory() {
        if (manipulator.getCategory(tmpCategoryNameInDialog) == null) {
            manipulator.renameCategory(categoryName, tmpCategoryNameInDialog);
            RequestContext.getCurrentInstance().closeDialog(null);
            tmpCategoryNameInDialog = null;
        } else if (tmpCategoryNameInDialog.equals(categoryName)){
            RequestContext.getCurrentInstance().closeDialog(null);
            tmpCategoryNameInDialog = null;
        } else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nazwa zajęta", ""));
        }
    }

    //Adding position
    public void addPositionFromDialog() {
        if (manipulator.getPosition(positionNameInDialog) != null){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Danie o tej nazwie już isynieje w bazie", ""));
        } else if (manipulator.getCategory(categoryName) == null){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nie ma takiej kategorii", ""));
        } else {
            manipulator.addPosition(positionNameInDialog, categoryName, imgSrc);
            logger.log(Level.INFO, "Added " + positionNameInDialog);
            positionNameInDialog = null;
            RequestContext.getCurrentInstance().closeDialog(null);
        } 
    }

    public String getPositionNameInDialog() {
        return positionNameInDialog;
    }

    public void setPositionNameInDialog(String positionNameInDialog) {
        this.positionNameInDialog = positionNameInDialog;
    }

    public String isCategoryNameSet() {
        if (categoryName == null || "".equals(categoryName)) {
            return "false";
        } else {
            return "true";
        }
    }

    public String getTmpCategoryNameInDialog() {
        return tmpCategoryNameInDialog;
    }

    public void setTmpCategoryNameInDialog(String tmpCategoryNameInDialog) {
        this.tmpCategoryNameInDialog = tmpCategoryNameInDialog;
    }

}
