/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tada.fcat.web.menuManager;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import tada.fcat.ejb.CategoriesManipulator;
import tada.fcat.entity.MenuPosition;
import tada.fcat.entity.PositionIngredient;

/**
 *
 * @author kudlaty524
 */
@Named(value = "editPosition")
@SessionScoped
public class EditPosition implements Serializable{

    @EJB
    CategoriesManipulator manipulator;

    public EditPosition() {
    }


    public void removeIngredient(MenuPosition position, PositionIngredient ingredient){
        manipulator.removeIngredientByPositionNameAndIngredientName(position.getPositionName(), ingredient.getIngredientName());
    }

 public List<PositionIngredient> getIngredientsByPosition(MenuPosition position){
     return manipulator.getIngredientsByPosition(position);
 }



}
