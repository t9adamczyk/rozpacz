/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tada.fcat.web.menuManager;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import tada.fcat.ejb.CategoriesManipulator;
import tada.fcat.entity.MenuCategory;

/**
 *
 * @author kudlaty524
 */
@Named(value = "addCategory")
@SessionScoped
public class AddCategory implements Serializable{
    private String categoryName;

    @EJB
    private CategoriesManipulator cm;
    private static final Logger logger = Logger.getLogger(AddCategory.class.getName());

    public AddCategory() {
    }
    
     public void addCategory(){
        if(cm.getCategory(categoryName) == null){
            cm.addCategory(categoryName);
            logger.log(Level.INFO,"Added "+categoryName);
            clear();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Kategoria o tej nazwie już istnieje", ""));
        }
                
        List<MenuCategory> lcm = cm.getCategories();
        for(MenuCategory mc : lcm){
            logger.log(Level.INFO,mc.toString());
        }
    }
     
    public void clear(){
        categoryName = null;
    }
    
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    
    
    
}
