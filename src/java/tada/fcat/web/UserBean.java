package tada.fcat.web;
 
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import tada.fcat.ejb.facades.UserFacade;
import tada.fcat.entity.User;
 

 
@SessionScoped
@ManagedBean(name = "userBean")
public class UserBean 
{
	
    private User user;
    
    private String currentPassword;
    private String newPassword;
 
    @EJB
    private UserFacade userFacade;
 
    public User getUser(){
        if(user == null){
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            String userLogin = context.getUserPrincipal().getName();
 
            user = userFacade.findUserByLogin(userLogin);
        }
        return user;
    }
 
    public boolean isUserAdmin(){
    	return user.getRole().equals("ADMIN");
    }
    
    public boolean isUserManager(){
    	return user.getRole().equals("MANAGER");
    }
    
    public boolean isUserCatering(){
    	return user.getRole().equals("CATERING");
    }
    
    public boolean canUserOrder(){
    	return user.getRole().equals("USER");
    }
 
    public String logOut(){
        getRequest().getSession().invalidate();
        return "logout";
    }
 
    private HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }
    
    public String changePassword()
    {
    	if(currentPassword.equals(user.getPassword()))
    		{
    			user.setPassword(newPassword);
    			userFacade.update(user);
    			sendInfoMessageToUser("Zmieniono has�o");
    			return "menu";
    		}
    	sendErrorMessageToUser("Obecne has�o niepoprawne");
    	return null;
    }

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String password) {
		this.currentPassword = password;
	}
	
	private void sendInfoMessageToUser(String message){
        FacesContext context = getContext();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));
    }
 
    private void sendErrorMessageToUser(String message){
        FacesContext context = getContext();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
    }
 
    private FacesContext getContext() {
        FacesContext context = FacesContext.getCurrentInstance();
        return context;
    }

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}