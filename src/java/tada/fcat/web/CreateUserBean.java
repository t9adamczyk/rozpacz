package tada.fcat.web;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import tada.fcat.ejb.facades.UserFacade;
import tada.fcat.entity.User;



@ManagedBean(name = "createUserBean")
@RequestScoped
public class CreateUserBean 
{
	private User user;
	
	@EJB
	private UserFacade userFacade;

	public User getUser() {
		
		if(user == null){
			user = new User();
        }
		
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String save()
	{
		user.setRole("USER");
		userFacade.save(user);
		return "login";
	}

}
