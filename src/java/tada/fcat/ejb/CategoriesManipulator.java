/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tada.fcat.ejb;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import tada.fcat.entity.MenuCategory;
import tada.fcat.entity.MenuPosition;
import tada.fcat.entity.PositionIngredient;

/**
 *
 * @author kudlaty524
 */
@Stateless
public class CategoriesManipulator {

    private static final Logger logger
            = Logger.getLogger("tada.fcat.ejb.CategoriesManipulator");

    @PersistenceContext
    private EntityManager em;

    public List<MenuCategory> getCategories() {
        List<MenuCategory> list = em.createNamedQuery("getAllCategories").getResultList();
        return list;
    }

    public void addCategory(String categoryName) {
        em.persist(new MenuCategory(categoryName));
    }

    public void removeCategory(String categoryName) {
        MenuCategory mc = getCategory(categoryName);
        em.remove(mc);
    }

    public MenuCategory getCategory(String categoryName) {
        TypedQuery<MenuCategory> query = em.createNamedQuery("getCategoryByCategoryName", MenuCategory.class).setParameter("categoryName", categoryName);
        List<MenuCategory> menuCategory = query.getResultList();
        if(menuCategory.isEmpty()) return null;
        else return menuCategory.get(0);
    }

    public void addPosition(String positionName, String categoryName, byte[] imgSrc) {
        MenuCategory mc = getCategory(categoryName);
        MenuPosition mp = new MenuPosition(positionName, mc, imgSrc);
        mc.getMenuPositionInCat().add(mp);
        em.merge(mc);
        em.persist(mp);
    }

    public void removePosition(String positionName) {
        MenuPosition menuPosition = getPosition(positionName);
        em.remove(menuPosition);
    }

    public void removePosition(MenuPosition menuPosition) {
        removePosition(menuPosition.getPositionName());
    }

    public MenuPosition getPosition(String positionName) {
         TypedQuery<MenuPosition> query = em.createNamedQuery("getPositionByPositionName", MenuPosition.class).setParameter("positionName", positionName);
        List<MenuPosition> menuPosition = query.getResultList();
        if(menuPosition.isEmpty()) return null;
        else return menuPosition.get(0);
    }

    public void addIngredient(String ingredientName, int ingredientGrams, String menuPositionName) {
        MenuPosition mp = getPosition(menuPositionName);
        PositionIngredient pi = new PositionIngredient(ingredientName, ingredientGrams, mp);
        mp.getIngredients().add(pi);
        em.merge(mp);
        em.persist(pi);
    }

    public void deletePosition(String positionName, String categoryName) {

    }

    public List<MenuPosition> getPositionsByCategoryName(String menuCategory) {
        TypedQuery<MenuPosition> query = em.createNamedQuery("getPositionsByCategoryName", MenuPosition.class).setParameter("menuCategory", menuCategory);
        return query.getResultList();
    }

    public void removeIngredientByPositionNameAndIngredientName(String positionName, String ingredientName) {
        TypedQuery<PositionIngredient> query = em.createNamedQuery("getIngredientByPositionNameAndIngredientName", PositionIngredient.class)
                .setParameter("positonName", positionName)
                .setParameter("ingredientName", ingredientName);
        em.remove(query.getSingleResult());
    }

    public boolean ifIngredientWithPositionNameAndIngredientNameExists(String positionName, String ingredientName) {
        TypedQuery<PositionIngredient> query = em.createNamedQuery("getIngredientByPositionNameAndIngredientName", PositionIngredient.class)
                .setParameter("positonName", positionName)
                .setParameter("ingredientName", ingredientName);
        return query.getResultList().isEmpty();
    }

    public void removeIngredient(PositionIngredient ingredient) {
        ingredient.getPosition().getIngredients().remove(ingredient);
        em.remove(ingredient);
    }

    public List<PositionIngredient> getIngredientsByPosition(MenuPosition position) {
        TypedQuery<PositionIngredient> query = em.createNamedQuery("getIngredientsByPosition", PositionIngredient.class)
                .setParameter("positonName", position.getPositionName());
        return query.getResultList();
    }
    
    public void renameCategory(String oldName, String newName){
        getCategory(oldName).setCategoryName(newName);
    }
}
