/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tada.fcat.ejb.dao;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateless;
import tada.fcat.entity.User;

@Stateless
public class UserDAO extends GenericDAO<User> {
 
    public UserDAO() {
        super(User.class);
    }
 
    public User findUserByLogin(String login){
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("login", login);     
 
        return super.findOneResult("User.findUserByLogin", parameters);
    }
}