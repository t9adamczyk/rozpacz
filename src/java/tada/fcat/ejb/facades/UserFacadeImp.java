package tada.fcat.ejb.facades;
 
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import tada.fcat.ejb.dao.UserDAO;
import tada.fcat.ejb.facades.UserFacade;
import tada.fcat.entity.User;


 
@Stateless
public class UserFacadeImp implements UserFacade {
 
    @EJB
    private UserDAO userDAO;
 
    @Override
    public void save(User user) {
        userDAO.save(user);
    }
 
    @Override
    public User update(User user) {
        return userDAO.update(user);
    }
 
    @Override
    public void delete(User user) {
        userDAO.delete(user, User.class);
    }
    
    @Override
    public void delete(int id) {
        userDAO.delete(id);
    }
 
   
    @Override
    public User find(int id) {
        return userDAO.find(id);
    }
 
    @Override
    public List<User> findAll() {
        return userDAO.findAll();
    }
 
    public User findUserByLogin(String login) {
        return userDAO.findUserByLogin(login);
    }
}