package tada.fcat.ejb.facades;
 
import java.util.List;

import javax.ejb.Local;
import tada.fcat.entity.User;


 
@Local
public interface UserFacade {
	
	public abstract void save(User user);
	 
    public abstract User update(User user);
 
    public abstract void delete(User user);
    
    public abstract void delete(int id);
 
    public abstract User find(int id);
 
    public abstract List<User> findAll();
	
    public User findUserByLogin(String login);
}